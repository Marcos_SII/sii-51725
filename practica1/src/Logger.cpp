 #include <sys/types.h>
 #include <sys/fcntl.h>           /* Definition of AT_* constants */
 #include <unistd.h>
 #include <sys/stat.h>
 #include <stdio.h>
 #include <iostream>
 #include <stdlib.h>
 #define MAX 60
  

 int main (void)
 {
 	int fd;
	char cadena[MAX];
	int create_error, read_error;
	
	unlink("/home/sii/tuberia");
	create_error=mkfifo("/home/sii/tuberia",0777);

	if(create_error==1)
	{
		perror("Error en la creación de la tubería\n");
		exit(1);
	}

	fd=open("/home/sii/tuberia",O_RDONLY);

	if(fd==-1)
	{
		perror("Error abriendo la tubería\n");
		exit(1);
	}

	printf("Inicio del juego \n\n");	
	
 	for(1;1;1)
 	{
 		read_error=read(fd,cadena,sizeof(cadena));

		if(read_error==-1)
		{
			perror("Error al leer la tubería\n");
			exit(1);
		}

		else if(cadena[0]=='F')
		{
			printf("%s",cadena);     //Pasamos al destructor de mundo el mensaje "Fin 							del juego"
			break;
		}

		else
		{
			printf("%s\n",cadena);
		}
 	}
 
 	close(fd);
 	unlink("/home/sii/tuberia");
 }	
