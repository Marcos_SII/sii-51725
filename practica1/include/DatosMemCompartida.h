#pragma once
#include "Raqueta.h"
#include "Esfera.h"

class DatosMemCompartida
{
 public:
	Raqueta raqueta1;
	Esfera esfera;
	int movimiento; //Si vale 0 no se mueve, si vale 1 sube y si vale -1 baja
};
