// Raqueta.h: interface for the Raqueta class.
//Fichero modificado por Marcos García Sánchez (51725)
//////////////////////////////////////////////////////////////////////

#pragma once
#include "Plano.h"
#include "Vector2D.h"

class Raqueta : public Plano  
{
public:
	Vector2D velocidad;

	Raqueta();
	virtual ~Raqueta();

	void Mueve(float t);
};
